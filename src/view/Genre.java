package view;

public enum Genre 
{
	All("All"),
	Action("Action"),
	Adventure("Adventure"),
	Animation("Animation"),
	Biography("Biography"),
	Comedy("Comedy"),
	Crime("Crime"),
	Documentary("Documentary"),
	Drama("Drama"),
	Family("Family"),
	Fantasy("Fantasy"),
	GameShow("Game-Show"),
	History("History"),
	Horror("Horror"),
	Music("Music"),
	Musical("Musical"),
	Mystery("Mystery"),
	News("News"),
	RealityTV("Reality-TV"),
	Romance("Romance"),
	SciFi("Sci-Fi"),
	Sitcom("Sitcom"),
	Sport("Sport"),
	TalkShow("Talk-Show"),
	Thriller("Thriller"),
	War("War"),	
	Western("Western");
	
	private String value;
	
	private Genre (String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;	
	}
	
	@Override
	public String toString()
	{
		return this.value;	
	}
}
