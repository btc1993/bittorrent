package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;

public class UpdateTorrentView extends JFrame 
{
	private ViewController viewController;	
	
	public UpdateTorrentView(ViewController viewController, String name, int downloadLimit, int uploadLimit)
	{
		this.viewController = viewController;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize(name, downloadLimit, uploadLimit);
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});		
	}

	private void updateTorrentInfo(String name, int downloadLimit, int uploadLimit) 
	{
		viewController.updateTorrentInfo(name, downloadLimit, uploadLimit);
		dispose();
	}

	private void initialize(String name, int downloadLimit, int uploadLimit) 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle(name);
        setBounds(250, 250, 325, 200);
        getContentPane().setLayout(null);
        
        JLabel lblSpeedLimits = new JLabel("Speed limits");
        lblSpeedLimits.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblSpeedLimits.setBounds(10, 11, 200, 20);
        getContentPane().add(lblSpeedLimits);
        
        JLabel lblDownloadLimit = new JLabel("Download speed limit:");
        lblDownloadLimit.setBounds(20, 42, 130, 20);
        getContentPane().add(lblDownloadLimit);
        
        JLabel lblUploadLimit = new JLabel("Upload speed limit: ");
        lblUploadLimit.setBounds(20, 73, 130, 20);
        getContentPane().add(lblUploadLimit);
        
        JSpinner spinnerDownload = new JSpinner();
        spinnerDownload.setModel(new SpinnerNumberModel(new Integer(0), new Integer(-1), null, new Integer(1)));
        spinnerDownload.setBounds(160, 42, 115, 20);
        spinnerDownload.setValue(downloadLimit);
        getContentPane().add(spinnerDownload);
        
        JSpinner spinnerUpload = new JSpinner();
        spinnerUpload.setModel(new SpinnerNumberModel(new Integer(0), new Integer(-1), null, new Integer(1)));
        spinnerUpload.setBounds(160, 73, 115, 20);
        spinnerUpload.setValue(uploadLimit);
        getContentPane().add(spinnerUpload);
        
        JButton btnApply = new JButton("Apply");
        btnApply.setBounds(87, 123, 89, 23);
        btnApply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateTorrentInfo(name, (int)spinnerDownload.getValue(), (int)spinnerUpload.getValue());
			}
		});
        getContentPane().add(btnApply);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
        btnCancel.setBounds(186, 123, 89, 23);
        getContentPane().add(btnCancel);
        
        setVisible(true);
	}
}

