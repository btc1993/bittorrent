package view;

public enum SortBy 
{
	title("title", "Title"),
	year("year", "Year"),
	rating("rating", "Rating"),
	peers("peers", "Peers"),
	seeds("seeds", "Seeds"),
	download_count("download_count", "Downloads"),
	date_count("date_count", "Date added");
	
	private String code;
	private String value;
	
	private SortBy(String code, String value)
	{
		this.code = code;
		this.value = value;
	}	
	
	public String getCode()
	{
		return this.code;
	}
	
	public String getValue()
	{
		return this.value;	
	}
	
	@Override
	public String toString()
	{
		return this.value;	
	}
}
