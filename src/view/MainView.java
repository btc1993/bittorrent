package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import com.frostwire.jlibtorrent.Pair;
import com.frostwire.jlibtorrent.SessionSettings;
import com.frostwire.jlibtorrent.TorrentHandle;

import domain.Confirm;
import domain.Error;
import domain.TorrentFileFilter;

public class MainView extends JFrame {
	
	private static final long serialVersionUID = -7676554640682339457L;

	private ViewController viewController;	
	private JPanel mainPanel;
	private HashMap<String,SubPanel> rows;
	private JScrollPane scrollPanel;
	private JPanel panel;
	private HashSet<String> selectedTorrents;
	
	private JMenuItem mntmPropieties;
	private JMenuItem mntmTorrentPreferences;
	private JMenuItem mntmStart;
	private JMenuItem mntmPause;
	private JMenuItem mntmRemove;	

	public MainView(ViewController viewController)
	{
		this.viewController = viewController;
		selectedTorrents = new HashSet<String>();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});			
	}
	
	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(250, 450);
	}

	/**
	 * Initialize the contents of the frame.
	*/
	private void initialize()
	{	
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				viewController.saveState();
				exitProcedure();
			}			
		});
        setBounds(250, 250, 643, 476);
        
        JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setMultiSelectionEnabled(false);
				fileChooser.setFileFilter(new TorrentFileFilter());
				int result = fileChooser.showOpenDialog(getParent());
				if (result == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					openTorrent(file);
				}	
			}
		});	
		mntmOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		mnFile.add(mntmOpen);
		
		JMenuItem mntmOpenURL = new JMenuItem("Open URL");
		mntmOpenURL.setEnabled(false);
		mnFile.add(mntmOpenURL);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CreateTorrentView(viewController);
			}
		});
		mnFile.add(mntmNew);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmStartAll = new JMenuItem("Start all");
		mntmStartAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				viewController.startAllTorrents();				
			}
		});	
		mnFile.add(mntmStartAll);
		
		JMenuItem mntmPauseAll = new JMenuItem("Pause all");
		mntmPauseAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				viewController.pauseAllTorrents();				
			}
		});	
		mnFile.add(mntmPauseAll);
		
		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		JMenuItem mntmSelectAll = new JMenuItem("Select all");
		mntmSelectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				for (Entry<String, SubPanel> row : rows.entrySet()) 
				{
					row.getValue().selectTorrent();		
				}		
			}
		});
		mnEdit.add(mntmSelectAll);
		
		JMenuItem mntmDeselectAll = new JMenuItem("Deselect all");
		mntmDeselectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				for (Entry<String, SubPanel> row : rows.entrySet()) 
				{
					row.getValue().deselectTorrent();		
				}		
			}
		});
		mnEdit.add(mntmDeselectAll);
		
		JSeparator separatorEdit = new JSeparator();
		mnEdit.add(separatorEdit);
		
		JMenuItem mntmPreferences = new JMenuItem("Preferences");
		mntmPreferences.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				SessionSettings settings  = viewController.getSessionSettings();
				new UpdateSessionSettingsView(viewController, settings.getDownloadRateLimit(), settings.getUploadRateLimit(), settings.getActiveDownloads(),
						settings.getActiveSeeds(), settings.getConnectionsLimit(), settings.getAnonymousMode(), settings.getSavePath());
			}
		});
		mnEdit.add(mntmPreferences);
		
		JMenu mnTorrent = new JMenu("Torrent");
		menuBar.add(mnTorrent);
		
		JMenuItem mntmSearch = new JMenuItem("Search torrent");
		mntmSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				new SearchTorrentsView(viewController);
			}
		});
		mnTorrent.add(mntmSearch);
		
		mntmPropieties = new JMenuItem("Properties");
		mntmPropieties.setEnabled(false);
		mntmPropieties.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				TorrentHandle result = viewController.getTorrentProperties(selectedTorrents.iterator().next());
				new TorrentInfoView(viewController, selectedTorrents.iterator().next(), result.getTorrentInfo().getTotalSize(), (int)(result.getStatus().getProgress()*100), 
						result.getStatus().getState().toString(), result.getStatus().getError(), result.getStatus().getListSeeds(), result.getSavePath(),
						result.getTorrentInfo().isPrivate(), result.getTorrentInfo().getCreator(), result.getTorrentInfo().getComment());
			}
		});
		mnTorrent.add(mntmPropieties);
		
		mntmTorrentPreferences = new JMenuItem("Preferences");
		mntmTorrentPreferences.setEnabled(false);
		mntmTorrentPreferences.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Pair<Integer,Integer> result = viewController.getTorrentInfo(selectedTorrents.iterator().next());
				new UpdateTorrentView(viewController, selectedTorrents.iterator().next(), result.first, result.second);
			}
		});
		mnTorrent.add(mntmTorrentPreferences);
		
		JMenuItem mntmOpenFolder = new JMenuItem("Open folder");
		mntmOpenFolder.setEnabled(false);
		mnTorrent.add(mntmOpenFolder);
		
		JSeparator separator_1 = new JSeparator();
		mnTorrent.add(separator_1);
		
		mntmStart = new JMenuItem("Start");
		mntmStart.setEnabled(false);
		mntmStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				viewController.startTorrents(new ArrayList<String>(selectedTorrents));				
			}
		});
		mnTorrent.add(mntmStart);
		
		mntmPause = new JMenuItem("Pause");
		mntmPause.setEnabled(false);
		mntmPause.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				viewController.pauseTorrents(new ArrayList<String>(selectedTorrents));			
			}
		});
		mnTorrent.add(mntmPause);
		
		JSeparator separator_2 = new JSeparator();
		mnTorrent.add(separator_2);
		
		mntmRemove = new JMenuItem("Remove");
		mntmRemove.setEnabled(false);
		mntmRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				deleteTorrents();
			}
		});
		mnTorrent.add(mntmRemove);
		
		JMenuItem mntmDelete = new JMenuItem("Delete files and remove torrent");
		mntmDelete.setEnabled(false);
		mnTorrent.add(mntmDelete);
        
        
        mainPanel = new JPanel();
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        rows = new HashMap<String,SubPanel>();
        
        scrollPanel = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPanel.setBounds(0, 59, 628, 312);
       
        scrollPanel.getPreferredSize();
        mainPanel.add(scrollPanel);

        panel = new JPanel();
        panel.setForeground(Color.PINK);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        scrollPanel.setViewportView(panel);    
	}	

	public void addTorrent(String name, long totalSize)
	{
		SubPanel newRow = new SubPanel(name, totalSize);
        rows.putIfAbsent(name, newRow);
        panel.add(newRow);
        scrollPanel.repaint();
        scrollPanel.revalidate();		
	}
	
	public void addTorrent(String name, int progress, long allTimeUpload,
			long allTimeDownload, long totalSize, int listPeers,
			int downloadRate, int uploadRate) 
	{
		addTorrent(name, totalSize);
		updateTorrent(name, progress, allTimeUpload, allTimeDownload, downloadRate, uploadRate, listPeers);
	}
	
	public void refreshProgress(String name, int progress)
	{
		SubPanel aux = rows.get(name);
		aux.getProgress().setValue(progress);		
	}
	
	public void updateTorrent(String name, int progress, long allTimeUpload,
			long allTimeDownload, int downloadRate, int uploadRate, int peers) 
	{
		SubPanel aux = rows.get(name);
		aux.getProgress().setValue(progress);
		aux.getLblAllTimeUpload().setText(readableFileSize(allTimeUpload));
		aux.getLblAllTimeDownload().setText(readableFileSize(allTimeDownload));
		aux.getLblUploadRate().setText(readableBandwidth(uploadRate));
		aux.getLblDownloadRate().setText(readableBandwidth(downloadRate));		
		aux.getLblListPeers().setText("Peers: " + peers);
	}
	
	private void addNewTorrent(File file)
	{
		System.out.println("Opening file" + file.getName());                            
		/*Pair<String, Long> torrent =*/
		viewController.addNewTorrent(file);	
		//addTorrent(torrent.first, 0, 0, 0, torrent.second, 0, 0, 0);
	}
	
	public void updatePeers(String name, int listPeers)
	{
		SubPanel aux = rows.get(name);
		aux.getLblListPeers().setText("Peers: " + listPeers);
	}
	
	
	/// Private methods		
	
	private void deleteTorrent ()
	{
		// Copiem la llista enun array per evitar un ConcurrentModificationException 
		// que podria ocorrer en aquesta aplicació multi-threading.
		ArrayList<String> deleteTorrents = new ArrayList<String>(selectedTorrents);
		for (String name : deleteTorrents) 
		{
			SubPanel row = rows.get(name);
	        rows.remove(name);
	        panel.remove(row);
	        selectedTorrents.remove(name);
		}
        scrollPanel.repaint();
        scrollPanel.revalidate();	
	}
	
	private void deleteTorrents()
	{
		int result = Confirm.showConfirmDialog(this, Confirm.CONFIRM1);
		if (result == JOptionPane.YES_OPTION)
		{
			viewController.deleteTorrents(new ArrayList<String>(selectedTorrents));	
			deleteTorrent();
		}		
	}
	
	private void exitProcedure() 
	{
		dispose();
		System.exit(0);
	}
	
	private void openTorrent(File file) 
	{
	    if (file.exists()) {
	    	if (file.getName().endsWith(".torrent")) 
	    	{
	    		addNewTorrent(file);				    
	    	}
	    	else 
	    	{
	    		Error.showMessageDialog(this, new ArrayList<Error>() {{ add(Error.ERROR3); }});
	    	}
		}
	}
	
	private void enableDisableMenuItems()
	{
		if (selectedTorrents.size() > 0)
		{
			mntmPropieties.setEnabled(true);
			mntmTorrentPreferences.setEnabled(true);
			mntmStart.setEnabled(true);
			mntmPause.setEnabled(true);
			mntmRemove.setEnabled(true);                			
		}
		else
		{
			mntmPropieties.setEnabled(false);
			mntmTorrentPreferences.setEnabled(false);
			mntmStart.setEnabled(false);
			mntmPause.setEnabled(false);
			mntmRemove.setEnabled(false);                			
		}
	}
	
	/// Private static methods
	
	private static String readableFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
	
	private static String readableBandwidth(long bandwidth)
	{
		if(bandwidth <= 0) return "0";
		final String[] units = new String[] { "B/s", "kB/s", "MB/s", "GB/s", "TB/s" };
		int digitGroups = (int) (Math.log10(bandwidth)/Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(bandwidth/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}	
	
	//////////////
	// Subpanel //
	/////////////
	
	private class SubPanel extends JPanel 
	{		
		private static final long serialVersionUID = 5459509853096457785L;

		private JLabel lblName;
		private JLabel lblSize;
		private JLabel lblAllTimeUpload;
		private JLabel lblAllTimeDownload;
		private JLabel lblListPeers;
		private JLabel lblDownloadRate;
		private JLabel lblUploadRate;
		private JProgressBar progress;
		
		private AtomicBoolean isSelected;       
		
        public SubPanel(String name, long totalSize) {
        	
           	isSelected = new AtomicBoolean();
        	setLayout(null);
        	
        	this.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent event) 
                {
                	if (!isSelected.get())
                	{
                		selectTorrent();                	
                	}
                	else
                	{
                		deselectTorrent();
                	} 
                }
        	});

            lblName = new JLabel(name);
            lblName.setBounds(10, 14, 250, 20);
            add(lblName);
            
            progress = new JProgressBar(0, 100);
            progress.setBounds(270, 14, 150, 20);
            add(progress);
            
            lblSize = new JLabel(readableFileSize(totalSize)); 
            lblSize.setBounds(10, 34, 50, 20);
            add(lblSize);
            
            lblAllTimeUpload = new JLabel (); //("Total Up: "+ 100 +"B");
            lblAllTimeUpload.setBounds(70, 34, 50, 20);
            add(lblAllTimeUpload);
            
            lblAllTimeDownload = new JLabel (); //("Total Down: "+ 100 +"B");
            lblAllTimeDownload.setBounds(120, 34, 50, 20);
            add(lblAllTimeDownload);
            
            lblListPeers = new JLabel ("Peers: 0");
            lblListPeers.setBounds(180, 34, 100, 20);
            add(lblListPeers);
            
            lblUploadRate = new JLabel (); //("Up: "+ 50 + "B/s");
            lblUploadRate.setBounds(250, 34, 50, 20);
            add(lblUploadRate);
            
            lblDownloadRate = new JLabel (); //("Down: "+ 50 + "B/s");
            lblDownloadRate.setBounds(310, 34, 50, 20);
            add(lblDownloadRate);
        }
        
        
        /**********************/
        /* Override functions */
        /**********************/
        
    	@Override
		public Dimension getPreferredSize() {
			return new Dimension(100, 50);
		}
        
    	/********************/
        /* Public functions */
    	/********************/
        public JProgressBar getProgress()
        {
        	return progress;
        }
                
        public JLabel getLblAllTimeUpload()
        {
        	return lblAllTimeUpload;
        }
        
        public JLabel getLblAllTimeDownload()
        {
        	return lblAllTimeDownload;
        }
        
		public JLabel getLblListPeers()
		{
			return lblListPeers;			
		}
		
		public JLabel getLblDownloadRate()
		{
			return lblDownloadRate;
		}
		
		public JLabel getLblUploadRate()
		{
			return lblUploadRate;
		}   
        
        public void selectTorrent()
        {
        	isSelected.set(true);
    		setBackground(Color.lightGray);
    		selectedTorrents.add(this.lblName.getText());       
        	enableDisableMenuItems();
        	System.out.println("Selected torrents: " + selectedTorrents.toString());

        }
        
        public void deselectTorrent()
        {
    		isSelected.set(false);
    		setBackground(new Color(240, 240, 240));
    		selectedTorrents.remove(this.lblName.getText());  
        	enableDisableMenuItems();
        	System.out.println("Selected torrents: " + selectedTorrents.toString());
        }
    }
}
