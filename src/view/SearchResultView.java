package view;

import java.awt.EventQueue;

import org.apache.http.client.utils.URIBuilder;

import com.frostwire.jlibtorrent.alerts.TorrentErrorAlert;

import domain.Error;
import domain.TorrentResponse;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Component;

import javax.swing.Box;
import javax.swing.SwingConstants;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SearchResultView extends JFrame
{
	private ViewController viewController;	
	private JTable table;
	private TorrentTableModel tableModel;
	
	public SearchResultView(ViewController viewController, ArrayList<TorrentResponse> result)
	{
		this.viewController = viewController;		
     	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
					fillTable(result);
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	
     	
	}
     	

	///
	// Private methods
	///
	
 	private void initialize()
	{
 		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setBounds(250, 250, 370, 345);
		this.setTitle("Search");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		tableModel = new TorrentTableModel(
				new String[] {"Title", "Year", "Rating", "Runtime", "Genre", "Language", 
						"Quality", "Seeds", "Peers", "Size", "URL", "Hash"}, 0);
			
		table = new JTable();
		table.setModel(tableModel);
		table.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e){
				if (e.getClickCount() == 2)
				{
					addSearchTorrent(table.getSelectedRow());
				}
			}
		});
		scrollPane.setViewportView(table);
		
		JPanel panelButtons = new JPanel();
		getContentPane().add(panelButtons, BorderLayout.SOUTH);
		
		JButton btnSearchAgain = new JButton("Search again");
		btnSearchAgain.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				new SearchTorrentsView(viewController);
				dispose();
			}
		});
		btnSearchAgain.setHorizontalAlignment(SwingConstants.RIGHT);
		JButton btnClose = new JButton("Close");
		
		btnClose.addActionListener(new ActionListener() 
        {
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
        });
		btnClose.setHorizontalAlignment(SwingConstants.RIGHT);
		FlowLayout fl_panelButtons = new FlowLayout(FlowLayout.CENTER, 5, 5);
		panelButtons.setLayout(fl_panelButtons);
		panelButtons.add(btnSearchAgain);
		panelButtons.add(btnClose);
		setVisible(true);				
	}
 	
	private void fillTable(ArrayList<TorrentResponse> result)
 	{
 		for (TorrentResponse torrentResponse : result) 
 		{
 			tableModel.addRow(new Object[] {
 					torrentResponse.getTitle(), torrentResponse.getYear(), torrentResponse.getRating(), torrentResponse.getRuntime(),
 					torrentResponse.getGenre(),	torrentResponse.getLanguage(), torrentResponse.getQuality(), torrentResponse.getSeeds(), 
 					torrentResponse.getPeers(),	torrentResponse.getSize(), torrentResponse.getUrl(), torrentResponse.getHash()
 			});			
		} 		
 	}
	
 	private void addSearchTorrent(int rowIndex) 
 	{
 		String url = table.getModel().getValueAt(rowIndex, table.getColumnModel().getColumnIndex("URL")).toString();
 		
 		String hash = table.getModel().getValueAt(rowIndex, table.getColumnModel().getColumnIndex("Hash")).toString();
 				
 		ArrayList<Error> errors = new ArrayList<Error>();
 		viewController.addSearchTorrent(url, hash, errors);
 		
 		if (errors.size() == 0)
 		{
 			// Dialog: the torrent was succesfully added! 			
 		}
 		else
 		{
 			// Return error
 			
 		}
	}
 	
 	
 	///
 	// TorrentTableModel class
 	//
 	
 	public class TorrentTableModel extends DefaultTableModel
 	{
 		public TorrentTableModel(Object[] columnNames, int rowCount)
 		{
 			super(columnNames, rowCount);
 		}
 		
 		@Override
 		public boolean isCellEditable(int row, int column)
 		{
 			return false;
 		}
 		
 		@Override
		public Class getColumnClass(int columnIndex) {
			return String.class;
		}
 		
 	}
}