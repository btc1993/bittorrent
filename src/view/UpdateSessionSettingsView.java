package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JTextField;

public class UpdateSessionSettingsView extends JFrame 
{
	private ViewController viewController;	
	private JTextField textSavePath;
	
	public UpdateSessionSettingsView(ViewController viewController, int downloadRateLimit, int uploadRateLimit, int activeDownloads, 
			int activeSeeds, int connectionsLimit, boolean isAnonymous, String savePath)
	{
		this.viewController = viewController;
     	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize(downloadRateLimit, uploadRateLimit, activeDownloads,	activeSeeds, connectionsLimit, isAnonymous, savePath);
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});				
	}
	

	private void updateSessionSettings(int downloadRateLimit, int uploadRateLimit, int activeDownloads, int activeSeeds, int connectionsLimit, 
			boolean isAnonymous, String savePath) 
	{
		viewController.updateSessionSettings(downloadRateLimit, uploadRateLimit, activeDownloads, activeSeeds, connectionsLimit, isAnonymous, savePath);
		dispose();
	}


	private void initialize(int downloadRateLimit, int uploadRateLimit, int activeDownloads, int activeSeeds, 
			int connectionsLimit, boolean isAnonymous, String savePath) 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(250, 250, 380, 400);
		getContentPane().setLayout(null);
		
		JLabel lblSpeedLimits = new JLabel("Speed limits");
		lblSpeedLimits.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSpeedLimits.setBounds(10, 66, 200, 20);
		getContentPane().add(lblSpeedLimits);
		
		JLabel lblDownloadRate = new JLabel("Download rate limit:");
		lblDownloadRate.setBounds(20, 92, 150, 20);
		getContentPane().add(lblDownloadRate);
		
		JLabel lblUploadRate = new JLabel("Upload rate limit:");
		lblUploadRate.setBounds(20, 118, 150, 20);
		getContentPane().add(lblUploadRate);
		
		JLabel lblConnectionLimits = new JLabel("Connection limits");
		lblConnectionLimits.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConnectionLimits.setBounds(10, 149, 200, 20);
		getContentPane().add(lblConnectionLimits);
		
		JLabel lblMaximumActiveDownloads = new JLabel("Maximum active downloads:");
		lblMaximumActiveDownloads.setBounds(20, 178, 150, 20);
		getContentPane().add(lblMaximumActiveDownloads);
		
		JLabel lblMaximumActiveSeeds = new JLabel("Maximum active seeds:");
		lblMaximumActiveSeeds.setBounds(20, 204, 150, 20);
		getContentPane().add(lblMaximumActiveSeeds);
		
		JLabel lblMaximumConnections = new JLabel("Maximum connections:");
		lblMaximumConnections.setBounds(20, 230, 150, 20);
		getContentPane().add(lblMaximumConnections);
		
		JLabel lblPrivacity = new JLabel("Privacity");
		lblPrivacity.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPrivacity.setBounds(10, 261, 200, 20);
		getContentPane().add(lblPrivacity);
		
		JCheckBox chckbxAnonymousMode = new JCheckBox("Anonymous mode");
		chckbxAnonymousMode.setSelected(isAnonymous);
		chckbxAnonymousMode.setBounds(20, 286, 150, 23);
		getContentPane().add(chckbxAnonymousMode);
				
		JSpinner spinnerDowloadRate = new JSpinner();
		spinnerDowloadRate.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinnerDowloadRate.setValue(downloadRateLimit);
		spinnerDowloadRate.setBounds(180, 92, 150, 20);
		getContentPane().add(spinnerDowloadRate);
		
		JSpinner spinnerUploadRate = new JSpinner();
		spinnerUploadRate.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinnerUploadRate.setValue(uploadRateLimit);
		spinnerUploadRate.setBounds(180, 118, 150, 20);
		getContentPane().add(spinnerUploadRate);
		
		JSpinner spinnerActiveDownloads = new JSpinner();
		spinnerActiveDownloads.setModel(new SpinnerNumberModel(new Integer(-1), new Integer(-1), null, new Integer(1)));
		spinnerActiveDownloads.setValue(activeDownloads);
		spinnerActiveDownloads.setBounds(180, 178, 150, 20);
		getContentPane().add(spinnerActiveDownloads);
		
		JSpinner spinnerActiveSeeds = new JSpinner();
		spinnerActiveSeeds.setModel(new SpinnerNumberModel(new Integer(-1), new Integer(-1), null, new Integer(1)));
		spinnerActiveSeeds.setValue(activeSeeds);
		spinnerActiveSeeds.setBounds(180, 204, 150, 20);
		getContentPane().add(spinnerActiveSeeds);
		
		JSpinner spinnerConnections = new JSpinner();
		spinnerConnections.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinnerConnections.setValue(connectionsLimit);
		spinnerConnections.setBounds(180, 230, 150, 20);
		getContentPane().add(spinnerConnections);
		
		JButton btnApply = new JButton("Apply");
		btnApply.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				updateSessionSettings((int)spinnerDowloadRate.getValue(), (int)spinnerUploadRate.getValue(), (int)spinnerActiveDownloads.getValue(),
						(int)spinnerActiveSeeds.getValue(), (int)spinnerConnections.getValue(), chckbxAnonymousMode.isSelected(), textSavePath.getText());
			}
		});
		btnApply.setBounds(136, 327, 89, 23);
		getContentPane().add(btnApply);
		
		JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new ActionListener() 
        {
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
        });
		btnCancel.setBounds(241, 327, 89, 23);
		getContentPane().add(btnCancel);
		
		JLabel lblDownloads = new JLabel("Downloads");
		lblDownloads.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDownloads.setBounds(10, 11, 200, 14);
		getContentPane().add(lblDownloads);
		
		JLabel lblDowloadsFolder = new JLabel("Dowloads folder:");
		lblDowloadsFolder.setBounds(20, 36, 150, 14);
		getContentPane().add(lblDowloadsFolder);
		
		textSavePath = new JTextField();
		textSavePath.setEditable(false);
		System.out.println("view Savepath = " + savePath);
		textSavePath.setText(savePath);
		textSavePath.setBounds(180, 33, 150, 20);
		getContentPane().add(textSavePath);
		textSavePath.setColumns(10);
		
		JButton btnSavePath = new JButton("...");
		btnSavePath.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int result = fileChooser.showOpenDialog(getParent()/*view.getTopWindow()*/);
				if (result == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile();
				    if (file.exists() && file.isDirectory()) 
				    {
				    	textSavePath.setText(file.getPath());
					}
				}	
			}
		});	
		btnSavePath.setBounds(330, 33, 25, 20);
		getContentPane().add(btnSavePath);
		
		setVisible(true);				
	}
}