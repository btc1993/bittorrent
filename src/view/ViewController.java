package view;

import java.io.File;
import java.util.ArrayList;

import com.frostwire.jlibtorrent.Pair;
import com.frostwire.jlibtorrent.SessionSettings;
import com.frostwire.jlibtorrent.TorrentHandle;

import domain.DomainController;
import domain.Error;
import domain.TorrentResponse;

public class ViewController {

	private DomainController domainController;
	private MainView mainView;
	
	public ViewController() throws Throwable 
	{
		mainView = new MainView(this);
		mainView.setVisible(true);
		domainController = new DomainController(this);
	}
	
	public DomainController getDomainController()
	{
		return domainController;
	}
	
	public void refreshProgress(String name, int progress)
	{
		mainView.refreshProgress(name, progress);
	}	
	
	public void addNewTorrent(File file)
	{
		domainController.addNewTorrent(file);
	}
	
	public void addTorrent (String name, long totalSize)
	{
		mainView.addTorrent(name, totalSize);
	}

	public void addTorrent(String name, int progress, long allTimeUpload, long allTimeDownload, long totalSize, 
			int listPeers, int downloadRate, int uploadRate) 
	{
		mainView.addTorrent(name, progress, allTimeUpload, allTimeDownload, totalSize, listPeers, downloadRate, uploadRate);
		
	}

	public void updateTorrent(String name, int progress, long allTimeUpload,
			long allTimeDownload, int downloadRate,
			int uploadRate, int peers) 
	{
		mainView.updateTorrent(name, progress, allTimeUpload, allTimeDownload, downloadRate, uploadRate, peers);		
	}

	public void updatePeers(String name, int listPeers) 
	{
		mainView.updatePeers(name, listPeers);		
	}

	public void startAllTorrents() 
	{
		domainController.startAllTorrents();		
	}

	public void pauseAllTorrents() 
	{
		domainController.pauseAllTorrents();
	}

	public void createTorrent(String saveTo, String sourceFile, String comment, boolean isPrivate, String trackers) 
	{
		domainController.createTorrent(saveTo, sourceFile, comment, isPrivate, trackers);		
	}

	public void pauseTorrents(ArrayList<String> selectedTorrents) 
	{
		domainController.pauseTorrents(selectedTorrents);		
	}

	public void startTorrents(ArrayList<String> selectedTorrents)
	{
		domainController.startTorrents(selectedTorrents);
	}

	public void deleteTorrents(ArrayList<String> selectedTorrents) 
	{
		domainController.deleteTorrents(selectedTorrents);
	}

	public void updateTorrentInfo(String name, int downloadLimit, int uploadLimit) 
	{
		domainController.updateTorrentInfo(name, downloadLimit, uploadLimit);
	}

	public Pair<Integer, Integer> getTorrentInfo(String name) 
	{
		return domainController.getTorrentInfo(name);
	}

	public void updateSessionSettings(int downloadRateLimit, int uploadRateLimit, int activeDownloads, int activeSeeds,
			int connectionsLimit, boolean isAnonymous, String savePath) 
	{
		domainController.updateSessionSettings(downloadRateLimit, uploadRateLimit, activeDownloads, activeSeeds, connectionsLimit, isAnonymous, savePath);
	}

	public SessionSettings getSessionSettings()
	{
		return domainController.getSessionSettings();
	}
	
	public void saveState()
	{
		domainController.saveState();
	}

	public TorrentHandle getTorrentProperties(String name) 
	{
		return domainController.getTorrentProperties(name);
	}

	public ArrayList<TorrentResponse> searchTorrents(String query, String genre, String quality, String sortBy, String orderBy, 
			String minRating, String maxResults, ArrayList<Error> errors) 
	{
		return domainController.searchTorrentsView(query, genre, quality, sortBy, orderBy, minRating, maxResults, errors);
	}

	public void addSearchTorrent(String url, String hash, ArrayList<Error> errors) 
	{
		domainController.addSearchTorrent(url, hash, errors);
	}
}