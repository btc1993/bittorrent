package view;

public enum Quality 
{
	QUALITY1("720p"),
	QUALITY2("1080P"),
	QUALITY3("3D");
	
	private String value;
	
	private Quality(String value)
	{
		this.value = value;
	}	
	
	public String getValue()
	{
		return this.value;	
	}
	
	@Override
	public String toString()
	{
		return this.value;	
	}
}
