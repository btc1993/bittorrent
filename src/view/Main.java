package view;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.UIManager;

import com.frostwire.jlibtorrent.Session;

public class Main {

	public static void main(String[] args) throws Throwable {		
		
		try {
			// Windows GUI.
	        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");         
		} 
		catch (Exception ex) {
			try {
				// JDK 6 Update 10 Cross platform GUI.
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");                    
			} 
			catch (Exception ex2) {
				try { 
					// Cross platform GUI for older java versions.
					UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
				} 
				catch (Exception ex3) {
				}
			}
		}
		
		ViewController viewController = new ViewController();
		
		// New event dispatcher thread. Creates a new run for the interface.
		/*javax.swing.SwingUtilities.invokeLater ( new Runnable() {
			@Override
			public void run() {
				ViewController viewController = new ViewController();
			}
		});*/
	}
}
		
		/*
		// Construct a session
		Session session = new Session();
		
		// Load session state
		File data = new File ("./settings.txt");   	
        
        try {
        	System.out.println(data.getCanonicalPath());
        	if (data.isFile())
        	{
        		System.out.println("File exist!");           		
        		Path path = Paths.get("./settings.txt");        		
        		session.loadState(Files.readAllBytes(path));

        	}
        	else {
        		System.out.println("Not exist!");
        	}
        	
        }
        catch (Exception e)
        {
        	e.printStackTrace();        	
        }
        
        // Start DHT, LSD, UPnP, NAT-PMP ...        
        System.out.println(session.isDHTRunning());
        System.out.println(session.isListening());
        
        
        // parse .torrent-files and add them to the sesssion
        
        
        // main loop
        DataInputStream input = new DataInputStream(System.in); 
        String string; 
        for (;;)
    	{
        	string = input.readLine();
        	switch (string) {
			case "add": 
				System.out.println("Adding...");
				String torrentPath = input.readLine();
				File torrent = new File (torrentPath);
				session.addTorrent(torrent, torrent.getParentFile());		
				
				break;

			default:
				break;
			}  
    	}       
	}
}*/
