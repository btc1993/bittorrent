package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

public class TorrentInfoView extends JFrame 
{
	private ViewController viewController;	
	private JTextField textTorrentSize;
	private JTextField textProgress;
	private JTextField textPeers;
	private JTextField textState;
	private JTextField textError;
	private JTextField textSavePath;
	private JTextField textPrivacity;
	private JTextField textCreator;
	private JTextField textComment;
	
	public TorrentInfoView(ViewController viewController, String name, long torrentSize, int progress, String state, String error, int peers,
			String savePath, boolean isPrivate, String creator, String comment)
	{
		this.viewController = viewController;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize(name, torrentSize, progress, state, error, peers, savePath, isPrivate, creator, comment);
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});				
	}

	
	/// Private methods
	
	private void initialize(String name, long torrentSize, int progress, String state, String error, int peers,
			String savePath, boolean isPrivate, String creator, String comment) 
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle(name);
        setBounds(250, 250, 360, 406);
		getContentPane().setLayout(null);
		
		JLabel lblActivity = new JLabel("Activity");
		lblActivity.setBounds(10, 10, 200, 20);
		lblActivity.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblActivity);
		
		JLabel lblTorrentSize = new JLabel("Torrent size:");
		lblTorrentSize.setBounds(20, 36, 100, 20);
		getContentPane().add(lblTorrentSize);
		
		JLabel lblProgress = new JLabel("Progress:");
		lblProgress.setBounds(20, 66, 100, 20);
		getContentPane().add(lblProgress);
		
		JLabel lblState = new JLabel("State:");
		lblState.setBounds(20, 126, 100, 20);
		getContentPane().add(lblState);
		
		JLabel lblError = new JLabel("Error:");
		lblError.setBounds(20, 156, 100, 20);
		getContentPane().add(lblError);
		
		JLabel lblPeers = new JLabel("Peers:");
		lblPeers.setBounds(20, 96, 100, 20);
		getContentPane().add(lblPeers);
		
		JLabel lblDetails = new JLabel("Details");
		lblDetails.setBounds(10, 182, 200, 20);
		lblDetails.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblDetails);
		
		JLabel lblSavePath = new JLabel("Save path:");
		lblSavePath.setBounds(20, 208, 100, 20);
		getContentPane().add(lblSavePath);
		
		JLabel lblPrivacity = new JLabel("Privacity:");
		lblPrivacity.setBounds(20, 238, 100, 20);
		getContentPane().add(lblPrivacity);
		
		JLabel lblCreator = new JLabel("Creator:");
		lblCreator.setBounds(20, 268, 100, 20);
		getContentPane().add(lblCreator);
		
		JLabel lblComment = new JLabel("Comment:");
		lblComment.setBounds(20, 298, 100, 20);
		getContentPane().add(lblComment);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(217, 333, 89, 23);
		
		getContentPane().add(btnClose);
		
		textTorrentSize = new JTextField(readableFileSize(torrentSize));
		textTorrentSize.setBounds(106, 36, 200, 20);
		textTorrentSize.setEditable(false);
		getContentPane().add(textTorrentSize);
		textTorrentSize.setColumns(10);
		
		textProgress = new JTextField(progress + "%");
		textProgress.setBounds(106, 66, 200, 20);
		textProgress.setEditable(false);
		getContentPane().add(textProgress);
		textProgress.setColumns(10);
		
		textPeers = new JTextField(String.valueOf(peers));
		textPeers.setBounds(106, 96, 200, 20);
		textPeers.setEditable(false);
		getContentPane().add(textPeers);
		textPeers.setColumns(10);
		
		textState = new JTextField(state);
		textState.setBounds(106, 126, 200, 20);
		textState.setEditable(false);
		getContentPane().add(textState);
		textState.setColumns(10);
		
		textError = new JTextField(error);
		textError.setBounds(106, 156, 200, 20);
		textError.setEditable(false);
		getContentPane().add(textError);
		textError.setColumns(10);
		
		textSavePath = new JTextField(savePath);
		textSavePath.setEditable(false);
		textSavePath.setBounds(106, 208, 200, 20);
		getContentPane().add(textSavePath);
		textSavePath.setColumns(10);
		
		textPrivacity = new JTextField(isPrivate ? "Private torret" : "Public torrent");
		textPrivacity.setEditable(false);
		textPrivacity.setBounds(106, 238, 200, 20);
		getContentPane().add(textPrivacity);
		textPrivacity.setColumns(10);
		
		textCreator = new JTextField(creator);
		textCreator.setEditable(false);
		textCreator.setBounds(106, 268, 200, 20);
		getContentPane().add(textCreator);
		textCreator.setColumns(10);
		
		textComment = new JTextField(comment);
		textComment.setEditable(false);
		textComment.setBounds(106, 298, 200, 20);
		getContentPane().add(textComment);
		textComment.setColumns(10);
		
		setVisible(true);
	}
	
	/// Private static methods
	
	private static String readableFileSize(long size) {
	    if(size <= 0) return "0";
	    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}