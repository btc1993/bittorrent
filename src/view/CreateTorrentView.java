package view;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridBagLayout;
import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import org.eclipse.swt.dnd.TextTransfer;

import com.frostwire.jlibtorrent.swig.tracker_alert;

import domain.Error;



public class CreateTorrentView extends JFrame
{
	
	private ViewController viewController;	
	private JTextField textSaveTo;
	private JTextField textSourceFile;
	private JTextField textComment;
	
	public CreateTorrentView(ViewController viewController)
	{
		this.viewController = viewController;
        EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});			
	}
	
	///
	//Private Methos
	///	
	
	private void createTorrent(String saveTo, String sourceFile, String comment, boolean isPrivate, String trackers) 
	{
		ArrayList <Error> errors = new ArrayList<Error>();
		validateInput(saveTo, sourceFile, trackers, errors);
		if (errors.size() == 0)
		{
			viewController.createTorrent(saveTo, sourceFile, comment, isPrivate, trackers);	
			dispose();
		}
		else
		{
			Error.showMessageDialog(this, errors);
		}
	}


	private void validateInput(String saveFile, String sourceFile, String trackers, ArrayList<Error> errors)
	{
		if (saveFile.isEmpty() || !(new File(saveFile).exists()))
		{
			errors.add(Error.ERROR1);
		}
		if (sourceFile.isEmpty() || !(new File(sourceFile).exists()))
		{
			errors.add(Error.ERROR2);
		}
	}			

	private void initialize()
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(250, 250, 480, 375);
        setVisible(true);

		getContentPane().setLayout(null);
		
		JLabel lblFiles = new JLabel("Files");
		lblFiles.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFiles.setBounds(10, 11, 46, 14);
		getContentPane().add(lblFiles);
		
		JLabel lblSaveTo = new JLabel("Save to:");
		lblSaveTo.setBounds(20, 36, 95, 14);
		getContentPane().add(lblSaveTo);
		
		JLabel lblSourceFile = new JLabel("Source file/folder:");
		lblSourceFile.setBounds(20, 66, 95, 14);
		getContentPane().add(lblSourceFile);
		
		JLabel lblProperties = new JLabel("Properties");
		lblProperties.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProperties.setBounds(10, 110, 100, 14);
		getContentPane().add(lblProperties);
		
		JLabel lblTrackers = new JLabel("Trackers:");
		lblTrackers.setBounds(20, 135, 95, 14);
		getContentPane().add(lblTrackers);
		
		JLabel lblComment = new JLabel("Comment:");
		lblComment.setBounds(20, 229, 95, 20);
		getContentPane().add(lblComment);
		
		JLabel lblPrivate = new JLabel("Private:");
		lblPrivate.setBounds(20, 263, 95, 20);
		getContentPane().add(lblPrivate);
		
		textSaveTo = new JTextField();
		textSaveTo.setEditable(false);
		textSaveTo.setBounds(150, 33, 250, 20);
		getContentPane().add(textSaveTo);
		textSaveTo.setColumns(10);
		
		textSourceFile = new JTextField();
		textSourceFile.setEditable(false);
		textSourceFile.setBounds(150, 63, 250, 20);
		getContentPane().add(textSourceFile);
		textSourceFile.setColumns(10);
		
		JButton btnSaveTo = new JButton("...");
		btnSaveTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int result = fileChooser.showOpenDialog(getParent()/*view.getTopWindow()*/);
				if (result == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile();
				    if (file.exists() && file.isDirectory()) 
				    {
				    	textSaveTo.setText(file.getPath());
					}
				}	
			}
		});	
		btnSaveTo.setBounds(399, 33, 25, 20);
		getContentPane().add(btnSaveTo);
		
		JButton btnSourceFile = new JButton("...");
		btnSourceFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				int result = fileChooser.showOpenDialog(getParent()/*view.getTopWindow()*/);
				if (result == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile();
				    if (file.exists() && (file.isDirectory() || file.isFile())) 
				    {
				    	textSourceFile.setText(file.getPath());
					}
				}	
			}
		});	
		btnSourceFile.setBounds(399, 63, 25, 20);
		getContentPane().add(btnSourceFile);
		
		JScrollPane scrollPaneTrackers = new JScrollPane();
		scrollPaneTrackers.setBounds(150, 135, 274, 80);
		getContentPane().add(scrollPaneTrackers);
		
		JEditorPane editorTrackers = new JEditorPane();
		scrollPaneTrackers.setViewportView(editorTrackers);
		
		textComment = new JTextField();
		textComment.setBounds(149, 229, 275, 20);
		getContentPane().add(textComment);
		textComment.setColumns(10);
		
		JCheckBox checkPrivate = new JCheckBox("");
		checkPrivate.setHorizontalAlignment(SwingConstants.TRAILING);
		checkPrivate.setBounds(150, 263, 20, 20);
		getContentPane().add(checkPrivate);
		
		JButton btnNew = new JButton("New");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createTorrent(textSaveTo.getText(), textSourceFile.getText(), textComment.getText(), checkPrivate.isSelected(), editorTrackers.getText());
			}
		});
		btnNew.setBounds(225, 290, 89, 23);
		getContentPane().add(btnNew);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnClose.setBounds(335, 290, 89, 23);
		getContentPane().add(btnClose);
		
		textSourceFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setMultiSelectionEnabled(false);
				int result = fileChooser.showOpenDialog(getParent()/*view.getTopWindow()*/);
				if (result == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
				    if (file.exists() && file.isDirectory()) {
				    	textSourceFile.setText(file.getPath());

					}
				    else {
				            //model.setStatus("File \"" + file.getName() + "\" does not exist");
				    }
				}	
			}
		});			
	}
}