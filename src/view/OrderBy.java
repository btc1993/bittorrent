package view;

public enum OrderBy {

	desc("desc", "Descending"),
	asc("asc", "Ascending");
	
	private String code;
	private String value;
	
	private OrderBy(String code, String value)
	{
		this.code = code;
		this.value = value;
	}
	
	public String getCode()
	{
		return this.code;
	}
	
	public String getValue()
	{
		return this.value;	
	}
	
	@Override
	public String toString()
	{
		return this.value;	
	}
}
