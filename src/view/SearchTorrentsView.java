package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;

import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.client.utils.URIBuilder;

import domain.Error;
import domain.TorrentResponse;

import javax.swing.DefaultComboBoxModel;

public class SearchTorrentsView extends JFrame
{
	private ViewController viewController;	
	private JTextField textQuery;
	
	public SearchTorrentsView(ViewController viewController)
	{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(250, 250, 345, 345);
		setTitle("Search");
		this.viewController = viewController;
		getContentPane().setLayout(null);
		
		JLabel lblQuery = new JLabel("Query:");
		lblQuery.setBounds(20, 40, 80, 20);
		getContentPane().add(lblQuery);
		
		JLabel lblSearchTorrents = new JLabel("Search torrents");
		lblSearchTorrents.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSearchTorrents.setBounds(10, 11, 200, 20);
		getContentPane().add(lblSearchTorrents);
		
		JLabel lblGenre = new JLabel("Genre:");
		lblGenre.setBounds(20, 70, 80, 20);
		getContentPane().add(lblGenre);
		
		JLabel lblQuality = new JLabel("Quality:");
		lblQuality.setBounds(20, 100, 80, 20);
		getContentPane().add(lblQuality);
		
		JLabel lblMiniRating = new JLabel("Min. rating:");
		lblMiniRating.setBounds(20, 190, 80, 20);
		getContentPane().add(lblMiniRating);
		
		JLabel lblOrderBy = new JLabel("Order by:");
		lblOrderBy.setBounds(20, 160, 80, 20);
		getContentPane().add(lblOrderBy);
		
		JLabel lblMaxResults = new JLabel("Max. results:");
		lblMaxResults.setBounds(20, 220, 80, 20);
		getContentPane().add(lblMaxResults);
		
		textQuery = new JTextField();
		textQuery.setBounds(95, 40, 200, 20);
		getContentPane().add(textQuery);
		textQuery.setColumns(10);
		
		JComboBox comboGenre = new JComboBox();
		comboGenre.setModel(new DefaultComboBoxModel(Genre.values()));
		comboGenre.setSelectedIndex(0);
		comboGenre.setBounds(95, 70, 200, 20);
		getContentPane().add(comboGenre);
		
		JComboBox comboQuality = new JComboBox();
		comboQuality.setModel(new DefaultComboBoxModel(Quality.values()));
		comboQuality.setBounds(95, 100, 200, 20);
		getContentPane().add(comboQuality);
		
		JSpinner spinnerRating = new JSpinner();
		spinnerRating.setModel(new SpinnerNumberModel(0, 0, 9, 1));
		spinnerRating.setBounds(95, 190, 200, 20);
		getContentPane().add(spinnerRating);
		
		JComboBox comboOrderBy = new JComboBox();
		comboOrderBy.setModel(new DefaultComboBoxModel(OrderBy.values()));
		comboOrderBy.setBounds(95, 160, 200, 20);
		getContentPane().add(comboOrderBy);
		
		JSpinner spinnerMaxResults = new JSpinner();
		spinnerMaxResults.setModel(new SpinnerNumberModel(20, 0, 50, 1));
		spinnerMaxResults.setBounds(95, 220, 200, 20);
		getContentPane().add(spinnerMaxResults);
		
		JLabel lblSortBy = new JLabel("Sort by:");
		lblSortBy.setBounds(20, 130, 46, 20);
		getContentPane().add(lblSortBy);
		
		JComboBox comboSortBy = new JComboBox();
		comboSortBy.setModel(new DefaultComboBoxModel(SortBy.values()));
		comboSortBy.setSelectedIndex(6);
		comboSortBy.setBounds(95, 130, 200, 20);
		getContentPane().add(comboSortBy);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() 
        {
			public void actionPerformed(ActionEvent e) 
			{
				dispose();
			}
        });
		btnCancel.setBounds(206, 270, 89, 23);
		getContentPane().add(btnCancel);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() 
        {
			public void actionPerformed(ActionEvent e) 
			{
				search(textQuery.getText(), comboGenre.getSelectedItem().toString(), comboQuality.getSelectedItem().toString(), 
						((SortBy)comboSortBy.getSelectedItem()).getCode(), ((OrderBy)comboOrderBy.getSelectedItem()).getCode(), 
						spinnerRating.getValue().toString(), spinnerMaxResults.getValue().toString());
			}
        });
		btnSearch.setBounds(106, 270, 89, 23);
		getContentPane().add(btnSearch);	

     	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
					//MainView window = new MainView();
					//window.frame.setVisible(true);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});				
	}
	
	///
	// Public methods
	///	


	
	
	/// 
	// Private methods
	///
	
	private void search(String query, String genre, String quality, String sortBy, String orderBy, String minRating, String maxResults) 
	{
		ArrayList <Error> errors = new ArrayList<Error>();
		ArrayList<TorrentResponse> result = viewController.searchTorrents(query, genre, quality, sortBy, orderBy, minRating, maxResults, errors);
		
		if (errors.size() == 0)
		{
			// Create and fill new table with search results
			new SearchResultView(viewController, result);
			
			// Close view
			dispose();			
		}
		else
		{
			// Read errror
			Error.showMessageDialog(this, errors);
		}
	}
	
	private void initialize()
	{
		setVisible(true);				
	}
}