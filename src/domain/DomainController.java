package domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import com.frostwire.jlibtorrent.Pair;
import com.frostwire.jlibtorrent.Session;
import com.frostwire.jlibtorrent.SessionSettings;
import com.frostwire.jlibtorrent.SettingsPack;
import com.frostwire.jlibtorrent.TorrentAlertAdapter;
import com.frostwire.jlibtorrent.TorrentHandle;
import com.frostwire.jlibtorrent.alerts.BlockFinishedAlert;
import com.frostwire.jlibtorrent.alerts.PeerConnectAlert;
import com.frostwire.jlibtorrent.alerts.PeerDisconnectedAlert;
import com.frostwire.jlibtorrent.alerts.TorrentAddedAlert;
import com.frostwire.jlibtorrent.alerts.TorrentFinishedAlert;
import com.frostwire.jlibtorrent.alerts.TorrentPausedAlert;
import com.frostwire.jlibtorrent.alerts.TorrentResumedAlert;
import com.frostwire.jlibtorrent.swig.char_vector;
import com.frostwire.jlibtorrent.swig.create_torrent;
import com.frostwire.jlibtorrent.swig.error_code;
import com.frostwire.jlibtorrent.swig.file_storage;
import com.frostwire.jlibtorrent.swig.libtorrent;
import com.frostwire.jlibtorrent.swig.torrent_handle;

import persistence.PersistenceController;
import view.ViewController;

public class DomainController {
	
	private PersistenceController persistenceController;
	private ViewController viewController;
	private Session session;
	private HashMap<String,String> torrentSource;
	
	public DomainController(ViewController viewController)
	{
		session = new Session();
		persistenceController = new PersistenceController(session);	
		torrentSource = new HashMap<String, String>();
		this.viewController = viewController;
		startDomainController();
	}	
	
	private void startDomainController()
	{
		// Load and add session settings
		SettingsPack sp = persistenceController.loadSessionSettings();	
		session.applySettings(sp);
		session.setSavePath(sp.getSavePath());
		System.out.println("start Savepath = " + session.getSavePath());
		
		// Load an add session stats
		Pair<Long, Long> stats = persistenceController.loadSessionStats();
		session.getStats().setTotalDownload(stats.first);
		session.getStats().setTotalUpload(stats.second);
			
		// Load and add torrents
		// parse .torrent-files and add them to the sesssion		        
		ArrayList<Torrent> torrents = persistenceController.loadTorrents(session.getSavePath());
		for (Torrent torrent : torrents) {
			viewController.addTorrent(torrent.getTorrentInfo().getName(), (int)torrent.getTorrentStatus().getProgress()*100,
					torrent.getTorrentStatus().getAllTimeUpload(), torrent.getTorrentStatus().getAllTimeDownload(), 
					torrent.getTorrentInfo().getTotalSize(), /*torrent.getTorrentStatus().getState(),*/ 
					torrent.getTorrentStatus().getListPeers(), torrent.getTorrentStatus().getDownloadRate(), 
					torrent.getTorrentStatus().getUploadRate());
			
			addTorrent(torrent.getSourcePath(), torrent.getSavePath());
		}
	}
	
	private Pair<String, Long> addTorrent(String sourcePath, String savePath)
	{		
		TorrentHandle th;		
		th = session.addTorrent(new File(sourcePath), new File(savePath));
		torrentSource.put(th.getName(), sourcePath);		
		
		System.out.println("Added " + th.getName() + " to session");
		
		th.setAutoManaged(true);
				
       	session.addListener(new TorrentAlertAdapter(th) {
        	
       		@Override
    		public void blockFinished(BlockFinishedAlert alert) 
       		{    			
    			viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds());    
    		}

    		@Override
    		public void torrentFinished(TorrentFinishedAlert alert)
    		{ 			
    			viewController.updateTorrent(th.getName(), 100, th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds()); 	    			
    		}            
         
            @Override
            public void torrentAdded(TorrentAddedAlert alert) 
            {
    			viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds()); 		
            }
            
            @Override
            public void torrentPaused(TorrentPausedAlert alert)
            {
            	viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), 0 /*th.getStatus().getDownloadRate()*/,
    					0 /*th.getStatus().getUploadRate()*/, 0 /*th.getStatus().getListSeeds()*/);
            }
            
            @Override
            public void torrentResumed(TorrentResumedAlert alert) 
            {
            	viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds());
            }
          
            @Override
            public void peerConnect(PeerConnectAlert alert) 
            {
    			viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds()); 		            
    		}            
            
            @Override
            public void peerDisconnected(PeerDisconnectedAlert alert) 
            {
    			viewController.updateTorrent(th.getName(), (int)(th.getStatus().getProgress()*100), th.getStatus().getAllTimeUpload(),
    					th.getStatus().getAllTimeDownload(), th.getStatus().getDownloadRate(),
    					th.getStatus().getUploadRate(), th.getStatus().getListSeeds()); 	
    		}
       });   	

       System.out.println("status name" + th.getStatus().getName() + "info name: " + th.getName());       
       
       th.resume();	
       
       return new Pair<String, Long>(th.getName(), (Long)th.getTorrentInfo().getTotalSize());
	}

	public void addNewTorrent(File file)
	{		
		persistenceController.addTorrent(file);
		//return addTorrent(file.getPath(), file.getParent());	
		System.out.println("addNewTorrent: " + file.getPath() +  " -- " + session.getSavePath());
		Pair<String, Long> result = addTorrent(file.getPath(), session.getSavePath());
		
		viewController.addTorrent(result.first, result.second);

	}	

	public void startAllTorrents()
	{
		for (TorrentHandle th : session.getTorrents()) 
		{
			start(th);
		}
	}	

	public void pauseAllTorrents()
	{
		for (TorrentHandle th : session.getTorrents()) 
		{
			pause(th);
		}
	}

	public void createTorrent(String saveTo, String sourceFile, String comment,	boolean isPrivate, String trackers) 
	{
		file_storage fs = new file_storage();

        // recursively adds files in directories
        libtorrent.add_files(fs, sourceFile);

        create_torrent t = new create_torrent(fs);
        t.add_tracker(trackers);
        t.set_creator("BTornent");
        t.set_comment(comment);
        t.set_priv(isPrivate);
        

        error_code ec = new error_code();
        // reads the files and calculates the hashes
        session.setPieceHashes(",,,", t, sourceFile, ec);

        if (ec.value() != 0) {
            System.out.println(ec.message());
        }

        System.out.println(t.generate().to_string());
        
        // Save .torrent file
        FileOutputStream fos;
		try 
		{
			File result = new File(saveTo+"\\"+fs.name()+".torrent");
			fos = new FileOutputStream(result);
	        char_vector buffer = t.generate().bencode();
	        
	        byte[] b = new byte[(int)buffer.capacity()];
	        for (int j = 0; j < b.length; j++) {
	        	b[j] = (byte)buffer.get(j);
	        }
	        
	        fos.write(b);
	        fos.close();    
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
    	
	}
	
	public void startTorrents(ArrayList<String> selectedTorrents) 
	{
		for (String torrent : selectedTorrents) 
		{		
			for (TorrentHandle th : session.getTorrents()) 
			{
				if (th.getName().equals(torrent))
				{
					System.out.println("started! " + th.getName());
					start(th);
				}
			}
		}	
	}

	public void pauseTorrents(ArrayList<String> selectedTorrents) 
	{		
		for (String torrent : selectedTorrents) 
		{		
			for (TorrentHandle th : session.getTorrents()) 
			{
				if (th.getName().equals(torrent))
				{
					System.out.println("paused! " + th.getName());
					pause(th);
				}
			}
		}		
	}	
	
	public void deleteTorrents(ArrayList<String> selectedTorrents) 
	{		
		for (String torrent : selectedTorrents) 
		{		
			for (TorrentHandle th : session.getTorrents()) 
			{
				if (th.getName().equals(torrent))
				{
					System.out.println("deleted! " + th.getName());
					delete(th);
				}
			}
		}		
	}
	
	//
	// Private functions
	//
	
	private void start(TorrentHandle th)
	{
		if (th.getStatus().isPaused())
		{
			th.setAutoManaged(true);
			th.resume();				
		}		
	}
	
	private void pause(TorrentHandle th) 
	{
		if (!th.getStatus().isPaused())
		{
			th.setAutoManaged(false);
			th.pause();				
		}	
	}
	
	private void delete(TorrentHandle th)
	{		
		System.out.println("save path --> " + torrentSource.get(th.getName()));
		persistenceController.deleteTorrent(torrentSource.get(th.getName()));
		torrentSource.remove(th.getName());
		session.removeTorrent(th);
	}

	public void updateTorrentInfo(String name, int downloadLimit, int uploadLimit) 
	{
		for (TorrentHandle th : session.getTorrents()) 
		{
			if (th.getName().equals(name))
			{
				th.setDownloadLimit(downloadLimit);
				th.setUploadLimit(uploadLimit);
			}
		}
	}

	public Pair<Integer, Integer> getTorrentInfo(String name) 
	{
		Pair<Integer, Integer> result = new Pair<Integer, Integer>(0, 0);
		for (TorrentHandle th : session.getTorrents()) 
		{
			if (th.getName().equals(name))
			{
				result.first = th.getDownloadLimit();
				result.second = th.getUploadLimit();
			}
		}
		return result;
	}

	public void updateSessionSettings(int downloadRateLimit, int uploadRateLimit, int activeDownloads, int activeSeeds,
			int connectionsLimit, boolean isAnonymous, String savePath) 
	{
		SettingsPack sp = new SettingsPack();
		sp.setDownloadRateLimit(downloadRateLimit);
		sp.setUploadRateLimit(uploadRateLimit);
		sp.setActiveDownloads(activeDownloads);
		sp.setActiveSeeds(activeSeeds);
		sp.setConnectionsLimit(connectionsLimit);
		sp.setAnonymousMode(isAnonymous);
		sp.setSavePath(savePath);
		session.applySettings(sp);
		session.getSettings().setSavePath(savePath);
		session.setSavePath(savePath);
	}

	public SessionSettings getSessionSettings() 
	{
		SessionSettings ss = session.getSettings();
		ss.setSavePath(session.getSavePath());
		return ss;
	}

	public void saveState() 
	{
		// Save session settings and session stats 
		SessionSettings ss = session.getSettings();
		ss.setSavePath(session.getSavePath());
		persistenceController.saveSessionSettings(ss);
		persistenceController.saveSessionStats(session.getStats());
	}

	public TorrentHandle getTorrentProperties(String name)
	{
		torrent_handle t = new torrent_handle();
		TorrentHandle result = new TorrentHandle(t);
		for (TorrentHandle th : session.getTorrents()) 
		{
			if (th.getName().equals(name))
			{
				result = th;
				break;				
			}
		}
		return result;
	}

	public ArrayList<TorrentResponse> searchTorrentsView(String query, String genre, String quality, String sortBy, String orderBy, String minRating, 
			String maxResults, ArrayList<Error> errors) 
	{
		ArrayList<TorrentResponse> result = new ArrayList<TorrentResponse>();
		// https://yts.to/api/v2/list_movies.json
		URIBuilder builder;
		try {
			builder = new URIBuilder("https://yts.to/api/v2/list_movies.json");
			builder.addParameter("query_term", query);
			builder.addParameter("genre", genre);
			builder.addParameter("quality", quality);
			builder.addParameter("sort_by", sortBy);
			builder.addParameter("order_by", orderBy);
			builder.addParameter("minimum_rating", minRating);
			builder.addParameter("limit", maxResults);		

			URL url = builder.build().toURL();
			
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			 
			// optional default is GET
			con.setRequestMethod("GET");
	 
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	 			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) 
			{
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			System.out.println(response.toString());
			
			// parsing the json response
			JSONObject jsonResponse = new JSONObject(response.toString());
			// print json 
			System.out.println("Json: \n" + jsonResponse.toString());
			
			if (jsonResponse.length() > 0)
			{
				if (jsonResponse.get("status").toString().equals("ok"))
				{
					System.out.println("status: "+ jsonResponse.get("status").toString());
					
					JSONObject data = jsonResponse.getJSONObject("data");
					JSONArray movies = data.getJSONArray("movies");
					for (int i = 0; i < movies.length(); i++)
					{
						TorrentResponse torrentResponse = new TorrentResponse();
						JSONObject movie = movies.getJSONObject(i);
						System.out.println("movie: " + movie.toString());
						torrentResponse.setTitle(movie.get("title").toString());
						torrentResponse.setYear(movie.get("year").toString());
						torrentResponse.setRating(movie.get("rating").toString());
						torrentResponse.setRuntime(movie.get("runtime").toString());
						torrentResponse.setGenre(movie.getJSONArray("genres").toString());
						torrentResponse.setLanguage(movie.get("language").toString());
			
						System.out.println("title: " + movies.getJSONObject(i).getString("title"));
						System.out.println("genres: " + movie.getJSONArray("genres").toString());
						
						if (movie.get("state").toString().equals("ok"))
						{
							JSONArray torrents = movie.getJSONArray("torrents");
							for (int j = 0; j < torrents.length(); j++)
							{
								JSONObject torrent = torrents.getJSONObject(j);
								torrentResponse.setUrl(torrent.get("url").toString());
								torrentResponse.setHash(torrent.get("hash").toString());
								torrentResponse.setQuality(torrent.get("quality").toString());
								torrentResponse.setSeeds(torrent.get("seeds").toString());
								torrentResponse.setPeers(torrent.get("peers").toString());
								torrentResponse.setSize(torrent.get("size").toString());
										
								TorrentResponse aux = new TorrentResponse(torrentResponse);
								result.add(aux);	
							}	
						}
					}
				}
				else 
				{
					// Error: Something was wrong, please try later					
				}
				
			}
			else
			{
				// Error: there's any response
			}
				    
		}	
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		for (TorrentResponse torrentResponse2 : result) {
			
			System.out.println(torrentResponse2.getQuality().toString());
			
		}
		return result;
	}

	public void addSearchTorrent(String url, String hash, ArrayList<Error> errors)
	{
		try 
		{
			// First we download the torrent file from the url
			File newTorrent = new File("AppData/Torrents/"+hash+".torrent");
			FileUtils.copyURLToFile(new URL(url), newTorrent, 8000, 8000);	// 8 seconds of timeout
			
			// Then, if there isn't any error, we add the new torrent to the session
			addNewTorrent(new File(newTorrent.getAbsolutePath()));
		} 
		catch (MalformedURLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		

	}
}
