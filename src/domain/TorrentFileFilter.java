package domain;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class TorrentFileFilter extends FileFilter
{
	public boolean accept(File file)
	{
		if (file.isDirectory())
		{
			return true;
		}
		return file.getName().toLowerCase().endsWith(".torrent");
	}	
	
	public String getDescription()
	{
		return "Torrent files (.torrent)";
	}
}