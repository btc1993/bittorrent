package domain;

import java.io.File;
import java.nio.file.Path;

import javax.xml.bind.annotation.XmlAttribute;

import com.frostwire.jlibtorrent.TorrentInfo;
import com.frostwire.jlibtorrent.TorrentStatus;
import com.frostwire.jlibtorrent.swig.torrent_status;

public class Torrent {

	@XmlAttribute
	private TorrentInfo torrentInfo;
	private TorrentStatus torrentStatus;
	private String sourcePath;
	private String savePath;
		
	public Torrent(File torrent, String sourcePath, String savePath)
	{
		torrentInfo = new TorrentInfo(torrent);
		torrent_status ts = new torrent_status();
		torrentStatus = new TorrentStatus(ts);
		this.sourcePath = sourcePath;
		this.savePath = savePath;
	}
	
	public TorrentInfo getTorrentInfo()
	{
		return torrentInfo;
	}
	
	public TorrentStatus getTorrentStatus()
	{
		return torrentStatus;
	}
	
	public String getSourcePath()
	{
		return sourcePath;
	}
	
	public String getSavePath()
	{
		return savePath;
	}	
}
