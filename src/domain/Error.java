package domain;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public enum Error 
{
	// Errors
	ERROR1("Invalid save file.", "Error", JOptionPane.ERROR_MESSAGE),
	ERROR2("Invalid source file/solder.", "Error", JOptionPane.ERROR_MESSAGE),
	ERROR3("Invalid file format, please select a .torrent file.", "Error", JOptionPane.ERROR_MESSAGE);
	
	
	private final String message;
	private final String title;
	private final int optionType;
	
	private Error(String message, String title, int optionType)
	{
		this.message = message;
		this.title = title;
		this.optionType = optionType;
	}	
		
	public String getMessage()
	{
		return this.message;
	}

	public String getTitle()
	{
		return this.title;
	}
	
	public int getOptionTye()
	{
		return this.optionType;
	}	
	
	public static void showMessageDialog(Component parentComponent, ArrayList<Error> errors)
	{
		if (errors.size() > 0)
		{
			String message = errors.get(0).getMessage();
			String title = errors.get(0).getTitle();
			int optionType = errors.get(0).getOptionTye();
			
			for (int i = 1; i < errors.size(); i++) 
			{
				message = message + "\n" + errors.get(i).getMessage();				
			}					
			
			JOptionPane.showMessageDialog(parentComponent, message, title, optionType );
		}
	}
}