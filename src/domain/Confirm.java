package domain;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public enum Confirm 
{
	
	// Confirmations
	CONFIRM1("Are you sure you want to delete these torrents?", "Delete opration", JOptionPane.YES_NO_OPTION);
	
	private final String message;
	private final String title;
	private final int optionType;
	
	private Confirm(String message, String title, int optionType)
	{
		this.message = message;
		this.title = title;
		this.optionType = optionType;
	}	
		
	public String getMessage()
	{
		return this.message;
	}

	public String getTitle()
	{
		return this.title;
	}
	
	public int getOptionTye()
	{
		return this.optionType;
	}	
	
	public static int showConfirmDialog(Component parentComponent, Confirm confirm)
	{
		return JOptionPane.showConfirmDialog(parentComponent, confirm.getMessage(), confirm.getTitle(), confirm.getOptionTye());
	}
}