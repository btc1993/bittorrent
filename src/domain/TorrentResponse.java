package domain;

public class TorrentResponse
{
	private String title;
	private String year;
	private String rating;
	private String runtime;
	private String genre;
	private String language;
	private String quality;
	private String seeds;
	private String peers;
	private String size;
	private String url;	
	private String hash;
	
	public TorrentResponse()
	{
				
	}
	
	public TorrentResponse(TorrentResponse torrentResponse)
	{
		this.title = torrentResponse.getTitle();
		this.year = torrentResponse.getYear();
		this.rating = torrentResponse.getRating();
		this.runtime = torrentResponse.getRuntime();
		this.genre = torrentResponse.getGenre();
		this.language = torrentResponse.getLanguage();
		this.quality = torrentResponse.getQuality();
		this.seeds = torrentResponse.getSeeds();
		this.peers = torrentResponse.getPeers();
		this.size = torrentResponse.getSize();
		this.url = torrentResponse.getUrl();	
		this.hash = torrentResponse.getHash();
	}

	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}

	public String getYear() 
	{
		return year;
	}

	public void setYear(String year) 
	{
		this.year = year;
	}

	public String getRating() 
	{
		return rating;
	}

	public void setRating(String rating) 
	{
		this.rating = rating;
	}

	public String getRuntime() 
	{
		return runtime;
	}

	public void setRuntime(String runtime) 
	{
		this.runtime = runtime;
	}

	public String getGenre() 
	{
		return genre;
	}

	public void setGenre(String genre) 
	{
		this.genre = genre;
	}

	public String getLanguage() 
	{
		return language;
	}

	public void setLanguage(String language) 
	{
		this.language = language;
	}

	public String getQuality() 
	{
		return quality;
	}

	public void setQuality(String quality)
	{
		this.quality = quality;
	}

	public String getSeeds() 
	{
		return seeds;
	}

	public void setSeeds(String seeds)
	{
		this.seeds = seeds;
	}

	public String getPeers()
	{
		return peers;
	}

	public void setPeers(String peers) {
		this.peers = peers;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public String getUrl() 
	{
		return url;
	}

	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getHash() 
	{
		return hash;
	}

	public void setHash(String hash) 
	{
		this.hash = hash;
	}
	

}
