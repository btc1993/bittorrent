package persistence;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.frostwire.jlibtorrent.Pair;
import com.frostwire.jlibtorrent.Session;
import com.frostwire.jlibtorrent.SessionSettings;
import com.frostwire.jlibtorrent.SessionStats;
import com.frostwire.jlibtorrent.SettingsPack;

import domain.Torrent;

public class PersistenceController {
	
	private DataController dataController;
	
	public PersistenceController(Session session)
	{
		dataController = new DataController();
		initializeSession(session);
	}
	
	public ArrayList<Torrent> loadTorrents(String savePath)
	{
		return dataController.loadTorrents(savePath);		
	}
	
	public SettingsPack loadSessionSettings()
	{
		return dataController.loadSessionSettings();		
	}
	
	public Pair<Long, Long> loadSessionStats()
	{
		return dataController.loadSessionStats();
	}
	
	public void saveSessionSettings(SessionSettings s)
	{
		dataController.saveSessionSettings(s);	
	}
	
	public void saveSessionStats(SessionStats s)
	{
		dataController.saveSessionStats(s);
	}
	
	private void initializeSession(Session session)
	{
		// Carreguem l'estat de la sessi� (si el tenim guardat)
		session.loadState(dataController.loadSessionState());
	
		/* Domain responsability
		// parse .torrent-files and add them to the sesssion		        
		ArrayList<Torrent> torrents = dataController.loadTorrents();
		for (Torrent torrent : torrents) {
			//File f = new File("C:/Users/Bernat/Downloads/"+torrent.getTorrentInfo().getName());
			session.addTorrent(new File(torrent.getSourcePath()), new File(torrent.getSavePath()));
			System.out.println("Added " + torrent.getTorrentInfo().getName() + " to session");
		}	
		*/	
	}
	
	public void saveSession(Session session)
	{
		// TO_DO
	}
	
	public void addTorrent(File file)
	{
		dataController.addTorrent(file);
	}

	public void deleteTorrent(String savePath)
	{
		dataController.deleteTorrent(savePath);
	}
}

