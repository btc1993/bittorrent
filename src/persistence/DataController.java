package persistence;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Savepoint;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.frostwire.jlibtorrent.Pair;
import com.frostwire.jlibtorrent.SessionProxy;
import com.frostwire.jlibtorrent.SessionSettings;
import com.frostwire.jlibtorrent.SessionStats;
import com.frostwire.jlibtorrent.SettingsPack;
import com.frostwire.jlibtorrent.swig.torrent_status;

import domain.Torrent;

public class DataController {	
	
	static String _STATE_PATH = "./AppData/state.txt";
	static String _TORRENTS_FOLDER_PATH = "./AppData/Torrents/";
	static String _TORRENTS_STATUS_PATH = "./AppData/torrent_status.xml";
	static String _SESSION_SETTINGS_PATH = "./AppData/session_settings.xml";
	static String _SESSION_STATS_PATH = "./AppData/session_stats.xml";
	
	
	public DataController()
	{
				
	}
	
	public byte[] loadSessionState()
	{	
		File stateFile = new File (_STATE_PATH);   
		Path statePath;
		byte[] session = new byte[0];
		try {
			if (stateFile.isFile())
			{
				System.out.println("File exist!");           		
				statePath = Paths.get(_STATE_PATH);        		
				session = new byte[(Files.readAllBytes(statePath)).length];
				session = Files.readAllBytes(statePath);
			}
			else {
				System.out.println("Not exist!");						
			}			
		}
		catch (Exception e)
		{
			e.printStackTrace();        	
		}	
		return session;
	}
	
	public ArrayList<Torrent> loadTorrents(String savePath)
	{
		ArrayList<Torrent> torrents = new ArrayList<Torrent>();
		
		// Create the folder where torrents are saved
		File torrentFolder = new File(_TORRENTS_FOLDER_PATH);
		torrentFolder.mkdirs();

		if (torrentFolder != null && torrentFolder.isDirectory()) {
            final File[] files = torrentFolder.listFiles();

            if (torrentFolder != null) {
                for (File f : files) {
                    if (f.isFile() && f.getName().endsWith(".torrent")) {
                    	File aux;
						try {
							aux = new File(f.getCanonicalPath());
							//torrent_status ts = getTorrentStatus(f.getName());							
							Torrent torrent = (new Torrent(aux, f.getCanonicalPath(), savePath));
							torrents.add(torrent);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					
					                    	
                    	//torrents.add(new Torrent());                        
                        System.out.println(f.getName() + ".torrent added.");
                    }
                }
            }
        }	
		
		return torrents;		
	}
	
	public void addTorrent(File file)
	{
		Path source = Paths.get(file.getAbsolutePath());
		Path target = Paths.get(_TORRENTS_FOLDER_PATH+file.getName());
		try {
			if (!target.toFile().exists()) 
			{
				Files.copy(source, target);
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	
	}
	
	public void deleteTorrent(String savePath)
	{
		Path path = Paths.get(savePath);
		try 
		{
			Files.deleteIfExists(path);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	
	public SettingsPack loadSessionSettings()
	{
		SettingsPack sp = new SettingsPack();
		
		try {

            File fXmlFile = new File(_SESSION_SETTINGS_PATH);
            if (fXmlFile.exists())
            {
            	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);     
                
                Element result = (Element)doc.getElementsByTagName("session_settings").item(0);
                sp.setActiveDownloads(Integer.parseInt(result.getElementsByTagName("activeDownloads").item(0).getTextContent()));
                sp.setActiveSeeds(Integer.parseInt(result.getElementsByTagName("activeSeeds").item(0).getTextContent()));
                sp.setAnonymousMode(Boolean.parseBoolean(result.getElementsByTagName("anonymousMode").item(0).getTextContent()));
                sp.setConnectionsLimit(Integer.parseInt(result.getElementsByTagName("connectionsLimit").item(0).getTextContent()));
                sp.setDownloadRateLimit(Integer.parseInt(result.getElementsByTagName("downloadRateLimit").item(0).getTextContent()));
                sp.setUploadRateLimit(Integer.parseInt(result.getElementsByTagName("uploadRateLimit").item(0).getTextContent()));
                sp.setSavePath(result.getElementsByTagName("savePath").item(0).getTextContent());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }	
		
		return sp;	
	}
	
	public Pair<Long, Long> loadSessionStats()
	{
		Pair<Long, Long> ss = new Pair<Long, Long>(Long.valueOf(0), Long.valueOf(0));
		
		try {

            File fXmlFile = new File(_SESSION_STATS_PATH);
            if (fXmlFile.exists())
            {
            	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);     
                
                Element result = (Element)doc.getElementsByTagName("session_stats").item(0);
                ss.first = Long.parseLong(result.getElementsByTagName("totalDownload").item(0).getTextContent());  
                ss.second = Long.parseLong(result.getElementsByTagName("totalUpload").item(0).getTextContent());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }			
		
		return ss;
	}
	
	public void saveSessionSettings(SessionSettings s) 
	{
		try 
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	       
	        // root element
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("session_settings");
	        doc.appendChild(root);
	        
	        // activeDownloads
	        Element activeDownloads = doc.createElement("activeDownloads");
	        activeDownloads.appendChild(doc.createTextNode(String.valueOf(s.getActiveDownloads())));
	        root.appendChild(activeDownloads);
	        
	        // activeSeeds
	        Element activeSeeds = doc.createElement("activeSeeds");
	        activeSeeds.appendChild(doc.createTextNode(String.valueOf(s.getActiveSeeds())));
	        root.appendChild(activeSeeds);
	        
	        // anonymousMode
	        Element anonymousMode = doc.createElement("anonymousMode");
	        anonymousMode.appendChild(doc.createTextNode(String.valueOf(s.getAnonymousMode())));
	        root.appendChild(anonymousMode);
	        
	        // connectionsLimit
	        Element connectionsLimit = doc.createElement("connectionsLimit");
	        connectionsLimit.appendChild(doc.createTextNode(String.valueOf(s.getConnectionsLimit())));
	        root.appendChild(connectionsLimit);
	        
	        // downloadRateLimit
	        Element downloadRateLimit = doc.createElement("downloadRateLimit");
	        downloadRateLimit.appendChild(doc.createTextNode(String.valueOf(s.getDownloadRateLimit())));
	        root.appendChild(downloadRateLimit);
	        
	        // uploadRateLimit
	        Element uploadRateLimit = doc.createElement("uploadRateLimit");
	        uploadRateLimit.appendChild(doc.createTextNode(String.valueOf(s.getUploadRateLimit())));
	        root.appendChild(uploadRateLimit);	
	        
	        // savePath
	        Element savePath = doc.createElement("savePath");
	        savePath.appendChild(doc.createTextNode(s.getSavePath()));
	        root.appendChild(savePath);
	        
	        // Write the content to xml file
	        TransformerFactory transformFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = new StreamResult(new File(_SESSION_SETTINGS_PATH));
	        
	        transformer.transform(source, result);
	        
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	
	public void saveSessionStats(SessionStats ss) 
	{
		try 
		{
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	       
	        // root element
	        Document doc = dBuilder.newDocument();
	        Element root = doc.createElement("session_stats");
	        doc.appendChild(root);
	        
	        // totalDownload
	        Element totalDownload = doc.createElement("totalDownload");
	        totalDownload.appendChild(doc.createTextNode(String.valueOf(ss.getTotalDownload())));
	        root.appendChild(totalDownload);
	        
	        // totalUpload
	        Element totalUpload = doc.createElement("totalUpload");
	        totalUpload.appendChild(doc.createTextNode(String.valueOf(ss.getTotalUpload())));
	        root.appendChild(totalUpload);
	        	        	        
	        // Write the content to xml file
	        TransformerFactory transformFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StreamResult result = new StreamResult(new File(_SESSION_STATS_PATH));
	        
	        transformer.transform(source, result);
	        
		} 
		catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	
//	private torrent_status getTorrentStatus(String name)
//	{
//		torrent_status ts = new torrent_status();
//				
//		try {
//
//            File fXmlFile = new File(_TORRENTS_STATUS_PATH);
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(fXmlFile);
//            
//            
//            XPathFactory factory = XPathFactory.newInstance();
//            XPath xpath = factory.newXPath();
//
//            // This XPath query will give you a list of only those <staff> elements
//            // which contain a <salary>200000</salary> element
//            XPathExpression expr = xpath.compile("//*[@id='"+name+"'][1]");
//            NodeList result = (NodeList)expr.evaluate(doc, XPathConstants.NODESET);
//
//            NodeList nodes = (NodeList) result;
//            for (int i = 0; i < nodes.getLength(); i++) {
//                Element eElement = (Element) nodes.item(i);
//                String id = eElement.getAttribute("id");
//                System.out.println("Download : " + eElement.getElementsByTagName("download").item(0).getTextContent());
//                System.out.println("Upload : " + eElement.getElementsByTagName("upload").item(0).getTextContent());
//                System.out.println("Completed time : " + eElement.getElementsByTagName("completedTime").item(0).getTextContent());
//                ts.setAll_time_download(Long.valueOf(eElement.getElementsByTagName("download").item(0).getTextContent()));
//            }
//
////            doc.getDocumentElement().normalize();
////            
////            Element eElement = doc.getElementById(name);
////            doc.
////            eElement.getAttributes();
////            eElement.getTextContent();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//		
//		return ts;
//	}
}
